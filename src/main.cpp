/**
 * @file main.cpp
 * @brief Template code for building with Shunya Stack
 * 
 * Compilation: Code comes with a cmake file, just run cmake 
 * 
 * Usage : Just run the command './main'
 */

/* --- Standard Includes --- */
#include <iostream>
#include <cstdlib>
#include <stdint.h>
#include <time.h> 
#include <unistd.h>
#include <errno.h>

#include <opencv2/opencv.hpp>

/* --- RapidJSON Includes --- */
/* MANDATORY: Allows to parse Shunya AI binaries output */
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/istreamwrapper.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/ostreamwrapper.h"
#include "rapidjson/filereadstream.h"

#include "subprocess.hpp" /* MANDATORY: Allows to run Shunya AI binaries */
#include "exutils.h" /* MANDATORY: Allows to parse Shunya AI binaries output */


/* --- Shunya Interfaces Includes --- */
#include <si/shunyaInterfaces.h> /* MANDATORY: Contains all IoT Functions */
#include <si/video.h>
#include <si/qrgen.h>
#include <si/scanner.h>
#include <si/whatsapp.h> 

using namespace std;
using namespace rapidjson;
using namespace cv;

int main(void)
{
    /* MANDATORY: Initializes the Shunya components */
    initLib();

    /* Generate QR code and store to output_qr.png */
    int8_t rc = makeQR("https://shunyaos.org", "output_qr.png");

    if (rc < 0) {
        std::cerr<<"Error! Failed to generate QR code"<<std::endl;
    }

    /* Read image using OpenCV */
    cv::Mat image = imread("output_qr.png");

    /* Find and read QR codes in the image */
    std::string qrData = readQR(image, SI_QRCODE);

    /* Check if the QR code was read successfully */
    if (qrData.compare(0, strlen("ERROR"), "ERROR") == 0) {
        std::cout<<"Error: " << qrData <<std::endl;

    } else {
        std::cout<<"QR Data: " << qrData <<std::endl;
    }

    /* Read image */
    cv::Mat image2 = imread("barcode.png");

    /* Find and read Barcodes of type CODE128 only */
    std::string barcodeData = readBarcode(image, SI_BARCODE_CODE128);

    if (barcodeData.compare(0, strlen("ERROR"), "ERROR") == 0) {
        std::cout<<"Error: " << barcodeData <<std::endl;

    } else {
        std::cout<<"Barcode Data: " << barcodeData <<std::endl;
    }
    
    /* Write your code here */
    return 0;
}
